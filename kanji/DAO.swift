//
//  Data.swift
//  kanji
//
//  Created by trent on 02/03/2017.
//  Copyright © 2017 Trent Milton. All rights reserved.
//

import UIKit
import SwiftyJSON

class DAO: NSObject {
    
    public var kanjis: [Kanji]!
    public var topWords: [TopWord]!
    
    override init() {
        
        kanjis = [Kanji]()
        if let jsonData = Helper.loadJson(filename: "kanji") {
            let json = JSON(data: jsonData)
            for (_, kanji):(String, JSON) in json["kanjis"] {
                let character = kanji["kanji"].stringValue
                let meanings = kanji["meanings"].arrayObject as? [String] ?? [String]()
                self.kanjis.append(Kanji(
                    character: character,
                    meanings: meanings
                ))
            }
        }
        
        topWords = [TopWord]()
        if let jsonData = Helper.loadJson(filename: "Japanese top 3000 words") {
            let json = JSON(data: jsonData)
            for topWord in json.array! {
                let position = topWord["Number"].int
                let english = topWord["English"].stringValue
                let japanese = topWord["Japanese"].stringValue
                self.topWords.append(TopWord(
                    position: position!,
                    japanese: japanese,
                    english: english
                ))
            }
        }
        
//        if let userName = json[0]["user"]["name"].string {
//            //Now you got your value
//        }
//        if let kanjis : [NSDictionary] = jsonResult["kanjis"] as? [NSDictionary] {
//            for kanji: NSDictionary in kanjis {
//                self.kanjis.append(Kanji(
//                    character: kanji["kanji"] as! String,
//                    meanings: kanji["meanings"] as? [String] ?? [String]()
//                ))
//            }
//        }
        
        
    }
    
}
