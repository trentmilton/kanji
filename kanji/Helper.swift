//
//  Helper.swift
//  kanji
//
//  Created by trent on 1/3/17.
//  Copyright © 2017 Trent Milton. All rights reserved.
//

import UIKit

class Helper: NSObject {
    
    static func loadJson(filename: String) -> Data? {
        if let path = Bundle.main.path(forResource: filename, ofType: "json") {
            do {
                let jsonData = try NSData(contentsOfFile: path, options: NSData.ReadingOptions.mappedIfSafe)
                return jsonData as Data
            } catch {}
        }
        return nil
    }
    
}
