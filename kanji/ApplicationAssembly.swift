//
//  ApplicationAssembly.swift
//  kanji
//
//  Created by trent on 02/03/2017.
//  Copyright © 2017 Trent Milton. All rights reserved.
//

import UIKit
import Typhoon

class ApplicationAssembly: TyphoonAssembly {
    
    
    public dynamic func kanjiListTVC() -> AnyObject {
        
        return TyphoonDefinition.withClass(KanjiListTVC.self, configuration: { (definition) in
            definition?.injectProperty(#selector(getter: KanjiListTVC.dao), with: self.dao())
        }) as AnyObject
    }
    
    public dynamic func topWordsTVC() -> AnyObject {
        
        return TyphoonDefinition.withClass(TopWordsTVC.self, configuration: { (definition) in
            definition?.injectProperty(#selector(getter: TopWordsTVC.dao), with: self.dao())
        }) as AnyObject
    }
    
    public dynamic func dao() -> AnyObject {
        return TyphoonDefinition.withClass(DAO.self) as AnyObject
    }
    
}
