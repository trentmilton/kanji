//
//  Kanji.swift
//  kanji
//
//  Created by trent on 1/3/17.
//  Copyright © 2017 Trent Milton. All rights reserved.
//

import UIKit

class Kanji: NSObject {

    var character: String
    var meanings: [String]
    
    init(character: String, meanings: [String]) {
        self.character = character
        self.meanings = meanings
    }
}
