//
//  TopWord.swift
//  kanji
//
//  Created by trent on 8/3/17.
//  Copyright © 2017 Trent Milton. All rights reserved.
//

import UIKit

class TopWord: NSObject {
    var position: Int
    var japanese: String
    var english: String
    
    init(position: Int, japanese: String, english: String) {
        self.position = position
        self.japanese = japanese
        self.english = english
    }
}
